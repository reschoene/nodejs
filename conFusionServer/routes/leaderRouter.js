const express = require('express');
const bodyParser = require('body-parser');
const authenticate = require('../authenticate');
const cors = require('./cors');

const Leaders = require('../models/leaders');

const leaderRouter = express.Router();

leaderRouter.use(bodyParser.json());

const retOkJson = (res, obj) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.json(obj);
}

leaderRouter.route('/')
.options(cors.corsWithOptions, (req, res) => {res.sendStatus(200);})
.get(cors.cors, async (req, res, next) => {
    try{
        let leaders = await Leaders.find({}).exec();

        retOkJson(res, leaders);  
    }
    catch(err){
        next(err);
    }
})

.post(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, async (req, res, next) => {
    try{
        let leader = await Leaders.create(req.body);
        console.log('Leader Created', leader);

        retOkJson(res, leader);
    }
    catch(err){
        next(err);
    }
})

.put(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
    res.statusCode = 403; //operation not supported
    res.end('PUT operation not supported on /leaders');
})

.delete(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, async (req, res, next) => {
    try{
        let resp = await Leaders.remove({});
        retOkJson(res, resp);
    }
    catch(err){
        next(err);
    }
});

leaderRouter.route('/:leaderId')
.options(cors.corsWithOptions, (req, res) => {res.sendStatus(200);})
.get(cors.cors, async (req, res, next) => {
    try {
        let leader = await Leaders.findById(req.params.leaderId);
        retOkJson(res, leader);
    }catch (err) {
        next(err);        
    }
})

.post(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
    res.statusCode = 403; //operation not supported
    res.end(`POST operation not supported on /leaders/${req.params.leaderId}`);
})

.put(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, async (req, res, next) => {
    try{
        let leader = await Leaders.findByIdAndUpdate(req.params.leaderId, {
            $set: req.body
        }, {
            new: true
        });

        retOkJson(res, leader);
    }catch(err){
        next(err);
    }
})

.delete(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, async (req, res, next) => {
    try{
        let leader = await Dishes.findByIdAndRemove(req.params.dishId);
        retOkJson(res, leader);
    }catch(err){
        next(err);
    }
});

module.exports = leaderRouter;