const express = require('express');
const bodyParser = require('body-parser');
const authenticate = require('../authenticate');
const cors = require('./cors');

const Promos = require('../models/promotions');

const promoRouter = express.Router();

promoRouter.use(bodyParser.json());

const retOkJson = (res, obj) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.json(obj);
}

promoRouter.route('/')
.options(cors.corsWithOptions, (req, res) => {res.sendStatus(200);})
.get(cors.cors, async (req, res, next) => {
    try{
        let promotions = await Promos.find({}).exec();

        retOkJson(res, promotions);
    }
    catch(err){
        next(err);
    }
})

.post(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, async (req, res, next) => {
    try{
        let promotion = await Promos.create(req.body);
        console.log('Promotion Created', promotion);

        retOkJson(res, promotion);
    }
    catch(err){
        next(err);
    }
})

.put(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
    res.statusCode = 403; //operation not supported
    res.end('PUT operation not supported on /promotions');
})

.delete(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, async (req, res, next) => {
    try{
        let resp = await Promos.remove({});
        retOkJson(res, resp);
    }
    catch(err){
        next(err);
    }
});

promoRouter.route('/:promoId')
.options(cors.corsWithOptions, (req, res) => {res.sendStatus(200);})
.get(cors.cors, async (req, res, next) => {
    try {
        let promotion = await Promos.findById(req.params.promoId);
        retOkJson(res, promotion);
    }catch (err) {
        next(err);        
    }
})

.post(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
    res.statusCode = 403; //operation not supported
    res.end(`POST operation not supported on /promotions/${req.params.promoId}`);
})

.put(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, async (req, res, next) => {
    try{
        let promotion = await Promos.findByIdAndUpdate(req.params.promoId, {
            $set: req.body
        }, {
            new: true
        });

        retOkJson(res, promotion);
    }catch(err){
        next(err);
    }
})

.delete(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, async (req, res, next) => {
    try{
        let promotion = await Promos.findByIdAndRemove(req.params.promoId);
        retOkJson(res, promotion);
    }catch(err){
        next(err);
    }
});

module.exports = promoRouter;