const express = require('express');
const bodyParser = require('body-parser');
const authenticate = require('../authenticate');
const cors = require('./cors');
const Favorite = require('../models/favorite');

const favoriteRouter = express.Router();

favoriteRouter.use(bodyParser.json());

favoriteRouter.route('/')
.options(cors.corsWithOptions, (req, res) => {res.sendStatus(200);})
.get(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    Favorite.find({user: req.user._id})
    .populate('dishes')
    .then((favorites) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(favorites);
    }, (err) => next(err))
    .catch((err) => next(err));
})

.post(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {    
    if (Favorite.findOne({user: req.user._id}, (err, favorite) => {
        if (err)
            next(err);
        else if (favorite == null){
            req.body.user = req.user._id;
            Favorite.create(req.body)
            .then((favorite) => {
                Favorite.findById(favorite._id)
                .populate('dishes')
                .populate('user')
                .then((favorite) => {
                    console.log('Favorite Created', favorite);
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(favorite);
                }, (err) => next(err))
                .catch((err) => next(err));
            })
            .catch((err) => next(err));
        }
        else{
            const dishes = req.body.dishes;
            for(var i=0; i<dishes.length; i++){
                if(favorite.dishes.indexOf(dishes[i]) === -1)
                    favorite.dishes.push(dishes[i]);
            }
            favorite.save()
            .then((favorite) => {
                Favorite.findById(favorite._id)
                .populate('dishes')
                .populate('user')
                .then((favorite) => {
                    console.log('Favorite Updated', favorite);
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(favorite);
                }, (err) => next(err))
                .catch((err) => next(err));                
            })
            .catch((err) => next(err));
        }
    }));
})

.put(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
    res.statusCode = 403; //operation not supported
    res.end('PUT operation not supported on /favorites');
})

.delete(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
    Favorite.remove({})
    .then((resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
    }, (err) => next(err))
    .catch((err) => next(err));
});

favoriteRouter.route('/:dishId')
.options(cors.corsWithOptions, (req, res) => {res.sendStatus(200);})
.post(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
    if (Favorite.findOne({user: req.user._id}, (err, favorite) => {
        if (err)
            next(err);
        else if (favorite == null){
            req.body.user = req.user._id;
            req.body.dishes = [req.params.dishId];
            Favorite.create(req.body)
            .then((favorite) => {
                Favorite.findById(favorite._id)
                .populate('dishes')
                .populate('user')
                .then((favorite) => {
                    console.log('Favorite Created', favorite);
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(favorite);
                }, (err) => next(err))
                .catch((err) => next(err));
            })
            .catch((err) => next(err));
        }
        else{
            if(favorite.dishes.indexOf(req.params.dishId) === -1)
                favorite.dishes.push(req.params.dishId);

            favorite.save()
            .then((favorite) => {
                Favorite.findById(favorite._id)
                .populate('dishes')
                .populate('user')
                .then((favorite) => {
                    console.log('Favorite Updated', favorite);
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(favorite);
                }, (err) => next(err))
                .catch((err) => next(err));                
            })
            .catch((err) => next(err));
        }
    }));
})

.put(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
    res.statusCode = 403; //operation not supported
    res.end(`PUT operation not supported on /favorites/${req.params.dishId}`);
})

.delete(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
    if (Favorite.findOne({user: req.user._id}, (err, favorite) => {
        if (err)
            next(err);
        else if (favorite == null){
            console.log('No favorites');
            res.statusCode = 204;
            res.setHeader('Content-Type', 'application/json');
            res.json({});
        }
        else{
            var idxToRemove = favorite.dishes.indexOf(req.params.dishId);
            if(idxToRemove > -1)
                favorite.dishes.splice(idxToRemove, 1);

            favorite.save()
            .then((favorite) => {
                Favorite.findById(favorite._id)
                .populate('dishes')
                .populate('user')
                .then((favorite) => {
                    console.log('Dish removed from favorite', favorite);
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(favorite);
                }, (err) => next(err))
                .catch((err) => next(err));                
            })
            .catch((err) => next(err));
        }
    }));
});

module.exports = favoriteRouter;